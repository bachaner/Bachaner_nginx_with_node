FROM nginx

LABEL maintainer="Oliger Timothee" \
			version="1.1"

ADD index.html /usr/share/nginx/html/index.html

RUN apt-get update && apt-get upgrade -y && apt-get install -y curl dialog apt-utils gnupg2

RUN curl -sL https://deb.nodesource.com/setup_9.x | bash - && apt-get install -y nodejs
